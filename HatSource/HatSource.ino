/*
 * Jo's Orbital Hat Source - WSU Commencement Spring 2016
 * 
 * This Arduino source is designed to use a hardware package of:
 *  Adafruit Feather Bluefruit LE - but only uses GPIO and USB power passthrough
 *  USB backup battery pack - this one is a 2200mAh
 *  Flora RGB Smart NeoPixel version 2 
 *  Adafruit NeoPixel Ring - 24 x 5050 RGB LED 
 *  4x NeoPixel 1/4 60 Ring - 5050 RGBW LED
 * 
 * These devices are connected and attached to a mortorboard hat for display during graduation commencement.
 *  Congratulations to Jo for getting her Masters of Applied Mathematics with a focus on Dynamical Systems from Washington State University!
 *   - Notably, her work is focused upon the motion of planetary bodies
 *   - This meant we needed a hat with a couple of orbiting planets
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 * 
 * Copyright 2016
 * Aaron S. Crandall
 * acrandal@gmail.com
 * 
 */
 
#include <TaskScheduler.h>
#include <Adafruit_NeoPixel.h>

#define PLANETS_PIN            6    // Control GPIO pin
#define PLANETS_NUMPIXELS      84   // 24 for inner ring, 60 for outer ring

/*******************************************************************
 * Earth settings 
 *  - On the inner orbit drawn by the smaller NeoPixel ring
 *  - Colors change slightly over time
 */
#define EARTH_LOC_BASE  0             // Neopixel ring address for Earth
#define EARTH_LOC_COUNT  24           //  24 is number of lights in inner ring
int g_Earth_curr_loc = 0;             // Earth initial NeoPixel address at boot

// Earth RGB values since they change with the orbit
int g_Earth_color_interval = 1;           // How quickly Earth shifts color tones

#define EARTH_RGB_RED_MIN 5           // Earth shifts colors a bit by changing red values
#define EARTH_RGB_RED_MAX 30
#define EARTH_RGB_GREEN 20
#define EARTH_RGB_BLUE 160

int g_Earth_curr_red = EARTH_RGB_RED_MIN;
int g_Earth_curr_green = EARTH_RGB_GREEN;
int g_Earth_curr_blue = EARTH_RGB_BLUE;


/*******************************************************************
 * Mars Settings 
 *  - On the outer orbit drawn by the large NeoPixel ring
 */
int g_Mars_curr_loc = 30;    // Mars initial NeoPixel address location on boot

#define MARS_LOC_BASE  24    // Neopixel ring address minimum location for Mars
                             //  24 is the first light in the outer ring
#define MARS_LOC_COUNT  60   //  Number of lights in the larger ring for the outer orbit


/*******************************************************************
 * Orbital velocity and periodicity settings/calculations
 */
#define EARTH_YEAR_MS  5000  // ms it takes Earth to circle the sun
#define MARS_YEAR_RATIO  1.8; // Ratio of Mars:Earth year for a cycle

int g_Earth_loc_timeout = EARTH_YEAR_MS / EARTH_LOC_COUNT;
int g_Mars_loc_timeout = (EARTH_YEAR_MS / MARS_LOC_COUNT) * MARS_YEAR_RATIO;


/*******************************************************************
 * Sun settings
 */
#define SUN_PIN 11
#define SUN_NUMPIXELS 1
#define SUN_BRIGHTNESS_MIN 70
#define SUN_BRIGHTNESS_MAX 140
#define SUN_SHINE_DELAY 100
int g_Sun_Interval = 1;
int g_Sun_Curr_Brightness = SUN_BRIGHTNESS_MIN;

// Initialize the neopixel pins and controllers
Adafruit_NeoPixel planet_pixels = Adafruit_NeoPixel(PLANETS_NUMPIXELS, PLANETS_PIN, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel sun_pixels = Adafruit_NeoPixel(SUN_NUMPIXELS, SUN_PIN, NEO_GRB + NEO_KHZ800);


/**************************************************************************************
 * Light controls are done via scheduler interrupts - TaskScheduler library
 *  - Begin defining tasks and scheduler setup
 */
void shineSun();
void moveEarth();
void moveMars();

Task sunTask(SUN_SHINE_DELAY, TASK_FOREVER, &shineSun);
Task earthTask(g_Earth_loc_timeout, TASK_FOREVER, &moveEarth);
Task marsTask(g_Mars_loc_timeout, TASK_FOREVER, &moveMars);

Scheduler runner;

/*********************************************************************************
 * Makes the sun dim and brighten the sun
 */
void shineSun() {
  sun_pixels.setPixelColor( 0, sun_pixels.Color(g_Sun_Curr_Brightness,
                                                g_Sun_Curr_Brightness,
                                                0) ); // dimish yellow
  sun_pixels.show();
  
  g_Sun_Curr_Brightness += g_Sun_Interval;
  if( g_Sun_Curr_Brightness >= SUN_BRIGHTNESS_MAX ){
    g_Sun_Interval *= -1;
  }
  else if( g_Sun_Curr_Brightness <= SUN_BRIGHTNESS_MIN ){
    g_Sun_Interval *= -1;
  }
}


/*************************************************************************
 * Moves earth around the inner orbit
 *  - Blank current light
 *  - Calculate address of next NeoPixel address
 *  - Draw Earth at new address
 */
void moveEarth() {
  planet_pixels.setPixelColor( g_Earth_curr_loc, planet_pixels.Color(0,0,0) ); // set off old light

  g_Earth_curr_loc += -1;
  if( g_Earth_curr_loc >= EARTH_LOC_BASE + EARTH_LOC_COUNT ){  // Overflow condition
    g_Earth_curr_loc = EARTH_LOC_BASE;
  }
  else if( g_Earth_curr_loc < EARTH_LOC_BASE ){ // Underflow condition
    g_Earth_curr_loc = EARTH_LOC_BASE + EARTH_LOC_COUNT - 1;
  }

  /* Cycle Earth through some various blues */
  g_Earth_curr_red += g_Earth_color_interval;
  if( g_Earth_curr_red <= EARTH_RGB_RED_MIN ){      // Hit lightest blue
    g_Earth_curr_red = EARTH_RGB_RED_MIN;
    g_Earth_color_interval *= -1;
  }
  else if( g_Earth_curr_red >= EARTH_RGB_RED_MAX ){ // Hit darkest blue
    g_Earth_curr_red = EARTH_RGB_RED_MAX;
    g_Earth_color_interval *= -1;
  }

  /* Draw Earth with the new location and color */
  planet_pixels.setPixelColor(  g_Earth_curr_loc, 
                                planet_pixels.Color(g_Earth_curr_red,
                                                    g_Earth_curr_green,
                                                    g_Earth_curr_blue) ); // set new light
  planet_pixels.show();
}


/***************************************************************************
 * Moves Mars around its orbit
 *  - Blank current NeoPixel
 *  - Calculate new NeoPixel address
 *  - Draw Mars at new address
 */
void moveMars() {
  planet_pixels.setPixelColor( g_Mars_curr_loc, planet_pixels.Color(0,0,0) ); // set off old light

  g_Mars_curr_loc += -1;  // Counter clockwise is -1
  if( g_Mars_curr_loc >= (MARS_LOC_BASE + MARS_LOC_COUNT) ){  // Overflow condition
    g_Mars_curr_loc = MARS_LOC_BASE;
  }
  else if( g_Mars_curr_loc < MARS_LOC_BASE ){ // Underflow condition
    g_Mars_curr_loc = MARS_LOC_BASE + MARS_LOC_COUNT - 1;
  }
  
  planet_pixels.setPixelColor( g_Mars_curr_loc, planet_pixels.Color(100,10,0) ); // set new light
  planet_pixels.show();  
}


/***************************************************************
 * Setting up tasks 
 */
void setup () {
  Serial.begin(9600);

  /* Initialize the lights */
  planet_pixels.begin(); // This initializes the NeoPixel library.
  planet_pixels.show();

  sun_pixels.begin();
  sun_pixels.show();

  /* Initialize the scheduler for light updates */
  runner.init();  
  runner.addTask(sunTask);
  runner.addTask(earthTask);
  runner.addTask(marsTask);
  delay(1000);
  
  sunTask.enable();
  earthTask.enable();
  marsTask.enable();
}

/***************************************************************
 * Every loop looks at current tasks in the TaskScheduler list to execute
 *  Otherwise, do nothing
 */
void loop () {
  runner.execute();
}
